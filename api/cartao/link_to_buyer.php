<?php
    include_once '../../domain/usecase/AssociaCartaoAoComprador.php';

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    // Instantiate dto object
    $dto = new stdClass();

    // Get raw posted data
    $data = json_decode(file_get_contents("php://input"));

    $dto->token = $data->token;
    $dto->customer = $data->customer;

    $usecase = new AssociaCartaoAoComprador();
    echo $usecase->executar($dto);