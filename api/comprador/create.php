<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    include_once '../../infra/dto/CompradorDto.php';
    include_once '../../domain/usecase/CadastroDeCompradorUseCase.php';

    // Instantiate dto object
    $dto = new CompradorDto();

    // Get raw posted data
    $data = json_decode(file_get_contents("php://input"));

    // TODO - Limpar a Controller passando o $data para o UseCase ao invés do $dto
    $dto->first_name = $data->first_name;
    $dto->last_name = $data->last_name;
    $dto->email = $data->email;
    $dto->phone_number = $data->phone_number;
    $dto->taxpayer_id = $data->taxpayer_id;
    $dto->birthdate = $data->birthdate;
    $dto->description = $data->description;
    $dto->address = $data->address;

    $usecase = new CadastroDeCompradorUseCase();
    echo $usecase->executar($dto);