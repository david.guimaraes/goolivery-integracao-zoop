<?php

    include_once '../../infra/dto/TransacaoDto.php';
    include_once '../../domain/usecase/CadastroDeTransacaoUseCase.php';

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    // Instantiate dto object
    $dto = new TransacaoDto();

    // Get raw posted data
    $data = json_decode(file_get_contents("php://input"));

    $dto->amount = $data->amount;
    $dto->currency = $data->currency;
    $dto->description = $data->description;
    $dto->payment_type = $data->payment_type;
    $dto->capture = $data->capture;
    $dto->on_behalf_of = $data->on_behalf_of;
    $dto->reference_id = $data->reference_id;
    $dto->payment_method = $data->payment_method;
    $dto->source = $data->source;
    $dto->installment_plan = $data->installment_plan;
    $dto->statement_descriptor = $data->statement_descriptor;
    $dto->customer = $data->customer;
    $dto->token = $data->token;
    $dto->metadata = $data->metadata;

    $usecase = new CadastroDeTransacaoUseCase();
    echo $usecase->executar($dto);
