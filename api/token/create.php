<?php
    include_once '../../domain/usecase/CadastroDeTokenParaCartaoUseCase.php';

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

    // Instantiate dto object
    $dto = new stdClass();

    // Get raw posted data
    $data = json_decode(file_get_contents("php://input"));

    $dto->holder_name = $data->holder_name;
    $dto->expiration_month = $data->expiration_month;
    $dto->expiration_year = $data->expiration_year;
    $dto->card_number = $data->card_number;
    $dto->security_code = $data->security_code;

    $usecase = new CadastroDeTokenParaCartaoUseCase();
    echo $usecase->executar($dto);
