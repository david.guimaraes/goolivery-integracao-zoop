<?php

include_once '../../../domain/usecase/CadastroDeVendedorDeTipoIndividuoUseCase.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

$data = json_decode(file_get_contents("php://input"));

$usecase = new CadastroDeVendedorDeTipoIndividuoUseCase();
// TODO - Verificar o retorno da Zoop e não apenas "ecoá-lo". A requisição aqui para a API de integração sempre será 200 Ok, mas nem sempre esse será o retorno da Zoop!
echo $usecase->executar($data);
