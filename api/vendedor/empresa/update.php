<?php

include_once '../../../domain/usecase/AlteracaoDeVendedorDeTipoEmpresaUseCase.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

// TODO - Verificar porque a função json_decode() simplesmente parou de funcionar nesse ponto
//$data = json_decode(file_get_contents("php://input"));
$data = file_get_contents("php://input");

$usecase = new AlteracaoDeVendedorDeTipoEmpresaUseCase();
echo $usecase->executar($data);
