<?php

include_once '../../../domain/usecase/ListagemDeDocumentosDeVendedoresUseCase.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

$idDoVendedor = $_GET['id'];

$usecase = new ListagemDeDocumentosDeVendedoresUseCase();

echo $usecase->executar($idDoVendedor);

