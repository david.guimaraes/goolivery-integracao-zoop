<?php

include_once '../../domain/usecase/DelecaoDeVendedorUseCase.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

$data = json_decode(file_get_contents("php://input"));

$idDoVendedorASerExcluido = $data->id;

$usecase = new DelecaoDeVendedorUseCase();

echo $usecase->executar($idDoVendedorASerExcluido);
