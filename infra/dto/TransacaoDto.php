<?php


class TransacaoDto
{
    public $id;                            //String
    public $resource;                      //String
    public $status;                        //Enum
    public $amount;                        //Integer
    public $original_amount;               //Integer
    public $currency;                      //String
    public $description;                   //String
    public $payment_type;                  //Enum
    public $transaction_number;            //String
    public $refunds;                       //List
    public $rewards;                       //List
    public $discounts;                     //String
    public $sales_receipt;                 //String
    public $on_behalf_of;                  //String
    public $customer;                      //String
    public $statement_descriptor;          //String
    public $payment_method ;               //Object
    public $installment_plan;              //Object
    public $mode;                          //Enum
    public $number_installments;           //Integer
    public $fees;                          //Integer
    public $fee_details;                   //Object
    public $location_latitute;             //Float
    public $location_longitude;            //Float
    public $metadata;                      //Object
    public $expected_on;                   //String
    public $created_at;                    //String
    public $updated_at;                    //String

    public $capture;                        //Boolean
    public $reference_id;                   //String
    public $source;                         //Source
    public $token;                          //String
}