<?php

include_once '../../domain/model/Address.php';

class CompradorDto
{
    public $id;                    //String
    public $status;                //String
    public $resource;              //String
    public $account_balance;       //Number
    public $current_balance;       //Number
    public $first_name;            //String
    public $last_name;             //String
    public $email;                 //String
    public $phone_number;          //String
    public $taxpayer_id;           //String
    public $birthdate;             //String
    public $description;           //String
    public $address;               //Object
    public $delinquent;            //Boolean
    public $default_debit;         //String
    public $default_credit;        //String
    public $metadata;              //Object
    public $created_at;            //String
    public $updated_at;            //String


}