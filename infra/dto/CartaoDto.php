<?php


class CartaoDto
{
    public $id;                            //String
    public $resource;                      //String
    public $description;                   //String
    public $card_brand;                    //String
    public $first4_digits;                 //String
    public $expiration_month;              //String
    public $expiration_year;               //String
    public $holder_name;                   //String
    public $is_active;                     //Boolean
    public $is_valid;                      //Boolean
    public $is_verified;                   //Boolean
    public $customer;                      //String
    public $fingerprint;                   //String
    public $address;                       //Object
    public $verification_checklist;        //Object
    public $metadata;                      //Object
    public $created_at;                    //String
    public $updated_at;                    //String
}