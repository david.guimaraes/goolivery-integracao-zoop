<?php

// TODO - Refatorar classe para remover duplicação de código
class AgenteDeComunicacaoExterna
{

    /**
     * AgenteDeComunicacaoExterna constructor.
     */
    public function __construct()
    {
    }

    public function executarComunicacao($url, $dto)
    {
        // TODO - Mover o token para um arquivo de configuração externo
        $token = 'zpk_prod_7gwctHjklOeYkZsAd1S5oLB1';
        $headers = array('Content-Type:application/json');
        $dados = json_encode($dto);

        //iniciando comunicacao
        $ch = curl_init();
        //URL
        curl_setopt($ch, CURLOPT_URL, $url);
        //definindo o tipo de arquivo
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //usando POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //enviando dados
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);
        //capturando o retorno
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //autenticacao
        curl_setopt($ch, CURLOPT_USERPWD, "$token");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //executando comunicacao
        $retorno = curl_exec($ch);
        //fechando conexao
        curl_close($ch);

        return $retorno;
    }

    public function executarGet($url)
    {
        // TODO - Mover o token para um arquivo de configuração externo
        $token = 'zpk_prod_7gwctHjklOeYkZsAd1S5oLB1';

        //iniciando comunicacao
        $ch = curl_init();
        //URL
        curl_setopt($ch, CURLOPT_URL, $url);
        //capturando o retorno
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //autenticacao
        curl_setopt($ch, CURLOPT_USERPWD, "$token");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //executando comunicacao
        $retorno = curl_exec($ch);
        //fechando conexao
        curl_close($ch);

        return $retorno;
    }

    public function executarDelete($url)
    {
        // TODO - Mover o token para um arquivo de configuração externo
        $token = 'zpk_prod_7gwctHjklOeYkZsAd1S5oLB1';
        $headers = array('Content-Type:application/json');

        //iniciando comunicacao
        $ch = curl_init();
        //usando DELETE
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        //URL
        curl_setopt($ch, CURLOPT_URL, $url);
        //definindo o tipo de arquivo
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //capturando o retorno
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //autenticacao
        curl_setopt($ch, CURLOPT_USERPWD, "$token");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //executando comunicacao
        $retorno = curl_exec($ch);
        //fechando conexao
        curl_close($ch);

        return $retorno;
    }

    public function executarPut($url, $dto)
    {
        // TODO - Mover o token para um arquivo de configuração externo
        $token = 'zpk_prod_7gwctHjklOeYkZsAd1S5oLB1';
        $headers = array('Content-Type:application/json');
        $dados = json_encode($dto);

        //iniciando comunicacao
        $ch = curl_init();
        //usando PUT
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        //URL
        curl_setopt($ch, CURLOPT_URL, $url);
        //definindo o tipo de arquivo
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //enviando dados
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);
        //capturando o retorno
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //autenticacao
        curl_setopt($ch, CURLOPT_USERPWD, "$token");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //executando comunicacao
        $retorno = curl_exec($ch);
        //fechando conexao
        curl_close($ch);

        return $retorno;
    }
}