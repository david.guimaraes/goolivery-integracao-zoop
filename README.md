# Goolivery-Zoop: Integração

Esse projeto consiste em uma RESTful API de integração entre o sistema [Goolivery](https://pedenaweb.com.br) da empresa [Engrene](https://engrene.com/) e a API de pagamentos da [Zoop](https://zoop.com.br/). 

# Getting Started

### Pré-requisitos

Para rodar este projeto, o único requerimento é um servidor de aplicação que ofereça suporte para a linguagem [PHP](https://www.php.net/). No caso, o servidor de aplicação utilizado foi o [XAMPP](https://www.apachefriends.org/pt_br/index.html) e o projeto foi desenvolvido no sistema operacional [Windows](https://www.microsoft.com/pt-br/windows/), da [Microsoft](https://www.microsoft.com/pt-br/). Embora o sistema operacional utilizado no desenvolvimento tenha sido o Windows, o servidor de aplicação em questão esta disponível para outros sistemas operacionais, como [Linux](https://linux.org/) e [macOS](https://www.apple.com/br/macos/catalina/). 

* [XAMPP](https://www.apachefriends.org/pt_br/index.html)

### Estrutura do Código

O código do projeto foi estruturado tendo em mente os princípios da [Clean Architecture](https://www.amazon.com.br/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164) de Robert C Martin, no entanto, a mesma não foi seguida "by the book". Foram criados as seguintes camadas:
* ...-infrastructure: equivalente à camada application da Clean Architecture. Contém códigos de infraestrutura, configurações, fabricação de objetos através do Factory Design Pattern;
* ...-domain: camada que contém toda a regra de negócio da aplicação. Aqui é só PHP! O código da regra de negócio é isolado para que, dentre outros motivos, não fique dependente de frameworks;
* ...-api: camada REST da aplicação. Equivalente à camada entrypoint da Clean Architecture.  