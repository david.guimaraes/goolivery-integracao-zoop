<?php


interface ICompradorDomainConverter
{
    public function fromDtoToModel($dto);

    public function fromModelToDto($model);
}