<?php

include_once 'ICompradorDomainConverter.php';
// TODO - Resolver o problema de caminhos relativos
require 'C:\xampp\htdocs\goolivery-integracao-zoop\domain\model\Comprador.php';

class CompradorDomainConverter implements ICompradorDomainConverter
{

    public function fromDtoToModel($dto)
    {
        return new Comprador($dto->id,
        $dto->status,
        $dto->resource,
        $dto->account_balance,
        $dto->current_balance,
        $dto->first_name,
        $dto->last_name,
        $dto->email,
        $dto->phone_number,
        $dto->taxpayer_id,
        $dto->birthdate,
        $dto->description,
        $dto->address,
        $dto->delinquent,
        $dto->default_debit,
        $dto->default_credit,
        $dto->metadata,
        $dto->created_at,
        $dto->updated_at);
    }

    public function fromModelToDto($model)
    {
        $dto = new CompradorDto();

        $dto->first_name = $model->getFirstName();
        $dto->last_name = $model->getLastName();
        $dto->email = $model->getEmail();
        $dto->phone_number = $model->getPhoneNumber();
        $dto->taxpayer_id = $model->getTaxpayerId();
        $dto->birthdate = $model->getBirthdate();
        $dto->description = $model->getDescription();
        $dto->address = $model->getAddress();

        return $dto;
    }
}