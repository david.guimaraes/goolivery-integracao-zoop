<?php


interface ICadastroDeVendedorDeTipoEmpresaUseCase
{
    public function executar($vendedorDeTipoEmpresaDto);
}