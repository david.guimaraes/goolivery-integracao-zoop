<?php

include_once 'IDelecaoDeVendedorUseCase.php';
require 'C:\xampp\htdocs\goolivery-integracao-zoop\gateway\AgenteDeComunicacaoExterna.php';

class DelecaoDeVendedorUseCase implements IDelecaoDeVendedorUseCase
{

    /**
     * DelecaoDeVendedorUseCase constructor.
     */
    public function __construct()
    {
    }

    public function executar($idDoVendedor)
    {
        // TODO - Resolver injeção de dependência ao invés de instanciar uma classe concreta
        $agenteDeComunicacaoExterna = new AgenteDeComunicacaoExterna();
        // TODO - Mover a URL para um arquivo de configuração externo
        $url = 'https://api.zoop.ws/v1/marketplaces/542a2eef59c342078deeaeb3af5272fb/sellers' . '/' . $idDoVendedor;

        return $agenteDeComunicacaoExterna->executarDelete($url);
    }
}