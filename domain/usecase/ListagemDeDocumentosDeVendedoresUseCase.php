<?php

include_once 'IListagemDeDocumentosDeVendedoresUseCase.php';
require 'C:\xampp\htdocs\goolivery-integracao-zoop\gateway\AgenteDeComunicacaoExterna.php';

class ListagemDeDocumentosDeVendedoresUseCase implements IListagemDeDocumentosDeVendedoresUseCase
{

    /**
     * ListagemDeDocumentosDeVendedoresUseCase constructor.
     */
    public function __construct()
    {
    }

    public function executar($idDoVendedor)
    {
        // TODO - Resolver injeção de dependência ao invés de instanciar uma classe concreta
        $agenteDeComunicacaoExterna = new AgenteDeComunicacaoExterna();
        // TODO - Mover a URL para um arquivo de configuração externo
        $url = 'https://api.zoop.ws/v1/marketplaces/542a2eef59c342078deeaeb3af5272fb/sellers' . '/' .
            $idDoVendedor . '/documents';

        return $agenteDeComunicacaoExterna->executarGet($url);
    }
}