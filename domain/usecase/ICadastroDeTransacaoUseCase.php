<?php


interface ICadastroDeTransacaoUseCase
{
    public function executar($transacaoDto);
}