<?php


interface ICadastroDeVendedorDeTipoIndividuoUseCase
{
    public function executar($vendedorDeTipoIndividuoDto);
}