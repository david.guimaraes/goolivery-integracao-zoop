<?php

include_once 'IAlteracaoDeVendedorDeTipoEmpresaUseCase.php';
require 'C:\xampp\htdocs\goolivery-integracao-zoop\gateway\AgenteDeComunicacaoExterna.php';

class AlteracaoDeVendedorDeTipoEmpresaUseCase implements IAlteracaoDeVendedorDeTipoEmpresaUseCase
{

    /**
     * AlteracaoDeVendedorDeTipoEmpresaUseCase constructor.
     */
    public function __construct()
    {
    }

    public function executar($vendedorDeTipoEmpresaDto)
    {
        // TODO - Resolver injeção de dependência ao invés de instanciar uma classe concreta
        $agenteDeComunicacaoExterna = new AgenteDeComunicacaoExterna();
        // TODO - Mover a URL para um arquivo de configuração externo
        // TODO - Alterar o id do Business. Concatenar com o que vier do corpo do dto. Não pode ficar chumbado!
        $url = 'https://api.zoop.ws/v1/marketplaces/542a2eef59c342078deeaeb3af5272fb/sellers/businesses/d016c3abe7834e6793b7a6c89417b4a6';

        return $agenteDeComunicacaoExterna->executarPut($url, $vendedorDeTipoEmpresaDto);
    }
}