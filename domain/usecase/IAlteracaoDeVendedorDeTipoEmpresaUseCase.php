<?php


interface IAlteracaoDeVendedorDeTipoEmpresaUseCase
{
    public function executar($vendedorDeTipoEmpresaDto);
}