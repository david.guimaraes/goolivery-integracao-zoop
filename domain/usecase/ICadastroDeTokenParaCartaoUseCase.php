<?php


interface ICadastroDeTokenParaCartaoUseCase
{
    public function executar($tokenDto);
}