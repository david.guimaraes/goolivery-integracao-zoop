<?php


interface IDelecaoDeVendedorUseCase
{
    public function executar($idDoVendedor);
}