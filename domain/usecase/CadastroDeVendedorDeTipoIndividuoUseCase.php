<?php

include_once 'ICadastroDeVendedorDeTipoIndividuoUseCase.php';
require 'C:\xampp\htdocs\goolivery-integracao-zoop\gateway\AgenteDeComunicacaoExterna.php';

class CadastroDeVendedorDeTipoIndividuoUseCase implements ICadastroDeVendedorDeTipoIndividuoUseCase
{

    /**
     * CadastroDeVendedorDeTipoIndividuoUseCase constructor.
     */
    public function __construct()
    {
    }

    public function executar($vendedorDeTipoIndividuoDto)
    {
        // TODO - Resolver injeção de dependência ao invés de instanciar uma classe concreta
        $agenteDeComunicacaoExterna = new AgenteDeComunicacaoExterna();
        // TODO - Mover a URL para um arquivo de configuração externo
        $url = 'https://api.zoop.ws/v1/marketplaces/542a2eef59c342078deeaeb3af5272fb/sellers/individuals';

        return $agenteDeComunicacaoExterna->executarComunicacao($url, $vendedorDeTipoIndividuoDto);
    }
}