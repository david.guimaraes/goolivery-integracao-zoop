<?php


interface IAlteracaoDeVendedorDeTipoIndividuoUseCase
{
    public function executar($vendedorDeTipoIndividuoDto);
}