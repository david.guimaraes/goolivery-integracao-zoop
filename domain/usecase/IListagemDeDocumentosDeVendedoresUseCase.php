<?php


interface IListagemDeDocumentosDeVendedoresUseCase
{
    public function executar($idDoVendedor);
}