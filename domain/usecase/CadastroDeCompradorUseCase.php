<?php

include_once 'ICadastroDeCompradorUseCase.php';
// TODO - Resolver o problema de caminhos relativos
require 'C:\xampp\htdocs\goolivery-integracao-zoop\domain\mapper\CompradorDomainConverter.php';
require 'C:\xampp\htdocs\goolivery-integracao-zoop\gateway\AgenteDeComunicacaoExterna.php';

class CadastroDeCompradorUseCase implements ICadastroDeCompradorUseCase
{

    /**
     * CadastroDeCompradorUseCase constructor.
     */
    public function __construct()
    {
    }

    public function executar($compradorDto)
    {
        // TODO - Resolver injeção de dependência ao invés de instanciar uma classe concreta
        $agenteDeComunicacaoExterna = new AgenteDeComunicacaoExterna();
        // TODO - Mover a URL para um arquivo de configuração externo
        $url = 'https://api.zoop.ws/v1/marketplaces/542a2eef59c342078deeaeb3af5272fb/buyers';

        return $agenteDeComunicacaoExterna->executarComunicacao($url, $compradorDto);
    }
}