<?php


class Address
{
    private $line1;             //string
    private $line2;             //string
    private $line3;             //string
    private $neighborhood;      //string
    private $city;              //string
    private $state;             //string Código ISO 3166-2 para o estado, com duas letras
    private $postal_code;       //string CEP com 8 digitos sem separador
    private $country_code;      //BR, nesse caso


    public function __construct()
    {

    }

    public static function fabricarNovoEnderecoComTodasAsPropriedades($line1, $line2, $line3, $neighborhood, $city, $state, $postal_code, $country_code)
    {
        $enderecoASerFabricado = new Address();
        $enderecoASerFabricado->line1 = $line1;
        $enderecoASerFabricado->line2 = $line2;
        $enderecoASerFabricado->line3 = $line3;
        $enderecoASerFabricado->neighborhood = $neighborhood;
        $enderecoASerFabricado->city = $city;
        $enderecoASerFabricado->state = $state;
        $enderecoASerFabricado->postal_code = $postal_code;
        $enderecoASerFabricado->country_code = $country_code;

        return $enderecoASerFabricado;
    }


    /**
     * @return mixed
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param mixed $line1
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;
    }

    /**
     * @return mixed
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param mixed $line2
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;
    }

    /**
     * @return mixed
     */
    public function getLine3()
    {
        return $this->line3;
    }

    /**
     * @param mixed $line3
     */
    public function setLine3($line3)
    {
        $this->line3 = $line3;
    }

    /**
     * @return mixed
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param mixed $neighborhood
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param mixed $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }

    /**
     * @param mixed $country_code
     */
    public function setCountryCode($country_code)
    {
        $this->country_code = $country_code;
    }      //string ISO 3166-1 alpha-2 - códigos de país de duas letras.


}