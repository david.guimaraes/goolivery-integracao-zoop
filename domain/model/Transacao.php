<?php


class Transacao
{
    private $id;                            //String
    private $resource;                      //String
    private $status;                        //Enum
    private $amount;                        //Integer
    private $original_amount;               //Integer
    private $currency;                      //String
    private $description;                   //String
    private $payment_type;                  //Enum
    private $transaction_number;            //String
    private $refunds;                       //List
    private $rewards;                       //List
    private $discounts;                     //String
    private $sales_receipt;                 //String
    private $on_behalf_of;                  //String
    private $customer;                      //String
    private $statement_descriptor;          //String
    private $payment_method ;               //Object
    private $installment_plan;              //Object
    private $mode;                          //Enum
    private $number_installments;           //Integer
    private $fees;                          //Integer
    private $fee_details;                   //Object
    private $location_latitute;             //Float
    private $location_longitude;            //Float
    private $metadata;                      //Object
    private $expected_on;                   //String
    private $created_at;                    //String
    private $updated_at;                    //String

    /**
     * Transacao constructor.
     * @param $amount
     * @param $original_amount
     * @param $currency
     * @param $description
     * @param $payment_type
     * @param $transaction_number
     * @param $refunds
     * @param $rewards
     * @param $discounts
     * @param $sales_receipt
     * @param $on_behalf_of
     * @param $customer
     * @param $statement_descriptor
     * @param $payment_method
     * @param $installment_plan
     * @param $mode
     * @param $number_installments
     * @param $fees
     * @param $fee_details
     * @param $location_latitute
     * @param $location_longitude
     */
    public function __construct($amount, $original_amount, $currency, $description, $payment_type, $transaction_number, $refunds, $rewards, $discounts, $sales_receipt, $on_behalf_of, $customer, $statement_descriptor, $payment_method, $installment_plan, $mode, $number_installments, $fees, $fee_details, $location_latitute, $location_longitude)
    {
        $this->amount = $amount;
        $this->original_amount = $original_amount;
        $this->currency = $currency;
        $this->description = $description;
        $this->payment_type = $payment_type;
        $this->transaction_number = $transaction_number;
        $this->refunds = $refunds;
        $this->rewards = $rewards;
        $this->discounts = $discounts;
        $this->sales_receipt = $sales_receipt;
        $this->on_behalf_of = $on_behalf_of;
        $this->customer = $customer;
        $this->statement_descriptor = $statement_descriptor;
        $this->payment_method = $payment_method;
        $this->installment_plan = $installment_plan;
        $this->mode = $mode;
        $this->number_installments = $number_installments;
        $this->fees = $fees;
        $this->fee_details = $fee_details;
        $this->location_latitute = $location_latitute;
        $this->location_longitude = $location_longitude;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getOriginalAmount()
    {
        return $this->original_amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->payment_type;
    }

    /**
     * @return mixed
     */
    public function getTransactionNumber()
    {
        return $this->transaction_number;
    }

    /**
     * @return mixed
     */
    public function getRefunds()
    {
        return $this->refunds;
    }

    /**
     * @return mixed
     */
    public function getRewards()
    {
        return $this->rewards;
    }

    /**
     * @return mixed
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    /**
     * @return mixed
     */
    public function getSalesReceipt()
    {
        return $this->sales_receipt;
    }

    /**
     * @return mixed
     */
    public function getOnBehalfOf()
    {
        return $this->on_behalf_of;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return mixed
     */
    public function getStatementDescriptor()
    {
        return $this->statement_descriptor;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * @return mixed
     */
    public function getInstallmentPlan()
    {
        return $this->installment_plan;
    }

    /**
     * @return mixed
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @return mixed
     */
    public function getNumberInstallments()
    {
        return $this->number_installments;
    }

    /**
     * @return mixed
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * @return mixed
     */
    public function getFeeDetails()
    {
        return $this->fee_details;
    }

    /**
     * @return mixed
     */
    public function getLocationLatitute()
    {
        return $this->location_latitute;
    }

    /**
     * @return mixed
     */
    public function getLocationLongitude()
    {
        return $this->location_longitude;
    }

    /**
     * @return mixed
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @return mixed
     */
    public function getExpectedOn()
    {
        return $this->expected_on;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param mixed $original_amount
     */
    public function setOriginalAmount($original_amount)
    {
        $this->original_amount = $original_amount;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $payment_type
     */
    public function setPaymentType($payment_type)
    {
        $this->payment_type = $payment_type;
    }

    /**
     * @param mixed $transaction_number
     */
    public function setTransactionNumber($transaction_number)
    {
        $this->transaction_number = $transaction_number;
    }

    /**
     * @param mixed $refunds
     */
    public function setRefunds($refunds)
    {
        $this->refunds = $refunds;
    }

    /**
     * @param mixed $rewards
     */
    public function setRewards($rewards)
    {
        $this->rewards = $rewards;
    }

    /**
     * @param mixed $discounts
     */
    public function setDiscounts($discounts)
    {
        $this->discounts = $discounts;
    }

    /**
     * @param mixed $sales_receipt
     */
    public function setSalesReceipt($sales_receipt)
    {
        $this->sales_receipt = $sales_receipt;
    }

    /**
     * @param mixed $on_behalf_of
     */
    public function setOnBehalfOf($on_behalf_of)
    {
        $this->on_behalf_of = $on_behalf_of;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param mixed $statement_descriptor
     */
    public function setStatementDescriptor($statement_descriptor)
    {
        $this->statement_descriptor = $statement_descriptor;
    }

    /**
     * @param mixed $payment_method
     */
    public function setPaymentMethod($payment_method)
    {
        $this->payment_method = $payment_method;
    }

    /**
     * @param mixed $installment_plan
     */
    public function setInstallmentPlan($installment_plan)
    {
        $this->installment_plan = $installment_plan;
    }

    /**
     * @param mixed $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @param mixed $number_installments
     */
    public function setNumberInstallments($number_installments)
    {
        $this->number_installments = $number_installments;
    }

    /**
     * @param mixed $fees
     */
    public function setFees($fees)
    {
        $this->fees = $fees;
    }

    /**
     * @param mixed $fee_details
     */
    public function setFeeDetails($fee_details)
    {
        $this->fee_details = $fee_details;
    }

    /**
     * @param mixed $location_latitute
     */
    public function setLocationLatitute($location_latitute)
    {
        $this->location_latitute = $location_latitute;
    }

    /**
     * @param mixed $location_longitude
     */
    public function setLocationLongitude($location_longitude)
    {
        $this->location_longitude = $location_longitude;
    }


}