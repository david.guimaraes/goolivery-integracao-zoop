<?php

include_once 'Address.php';

class Comprador
{
    private $id;                    //String
    private $status;                //String
    private $resource;              //String
    private $account_balance;       //Number
    private $current_balance;       //Number
    private $first_name;            //String
    private $last_name;             //String
    private $email;                 //String
    private $phone_number;          //String
    private $taxpayer_id;           //String
    private $birthdate;             //String
    private $description;           //String
    private $address;               //Object
    private $delinquent;            //Boolean
    private $default_debit;         //String
    private $default_credit;        //String
    private $metadata;              //Object
    private $created_at;            //String
    private $updated_at;            //String

    /**
     * Comprador constructor.
     * @param $id
     * @param $status
     * @param $resource
     * @param $account_balance
     * @param $current_balance
     * @param $first_name
     * @param $last_name
     * @param $email
     * @param $phone_number
     * @param $taxpayer_id
     * @param $birthdate
     * @param $description
     * @param $address
     * @param $delinquent
     * @param $default_debit
     * @param $default_credit
     * @param $metadata
     * @param $created_at
     * @param $updated_at
     */
    public function __construct($id, $status, $resource, $account_balance, $current_balance, $first_name, $last_name, $email, $phone_number, $taxpayer_id, $birthdate, $description, $address, $delinquent, $default_debit, $default_credit, $metadata, $created_at, $updated_at)
    {
        $this->id = $id;
        $this->status = $status;
        $this->resource = $resource;
        $this->account_balance = $account_balance;
        $this->current_balance = $current_balance;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->phone_number = $phone_number;
        $this->taxpayer_id = $taxpayer_id;
        $this->birthdate = $birthdate;
        $this->description = $description;
        $this->address = $address;
        $this->delinquent = $delinquent;
        $this->default_debit = $default_debit;
        $this->default_credit = $default_credit;
        $this->metadata = $metadata;
        $this->created_at = $created_at;
        $this->updated_at = $updated_at;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return mixed
     */
    public function getAccountBalance()
    {
        return $this->account_balance;
    }

    /**
     * @return mixed
     */
    public function getCurrentBalance()
    {
        return $this->current_balance;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @return mixed
     */
    public function getTaxpayerId()
    {
        return $this->taxpayer_id;
    }

    /**
     * @return mixed
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getDelinquent()
    {
        return $this->delinquent;
    }

    /**
     * @return mixed
     */
    public function getDefaultDebit()
    {
        return $this->default_debit;
    }

    /**
     * @return mixed
     */
    public function getDefaultCredit()
    {
        return $this->default_credit;
    }

    /**
     * @return mixed
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $phone_number
     */
    public function setPhoneNumber($phone_number)
    {
        $this->phone_number = $phone_number;
    }

    /**
     * @param mixed $taxpayer_id
     */
    public function setTaxpayerId($taxpayer_id)
    {
        $this->taxpayer_id = $taxpayer_id;
    }

    /**
     * @param mixed $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }


}