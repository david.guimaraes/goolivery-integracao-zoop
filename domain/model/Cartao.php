<?php

include_once 'Address.php';

class Cartao
{
    private $id;                            //String
    private $resource;                      //String
    private $description;                   //String
    private $card_brand;                    //String
    private $first4_digits;                 //String
    private $expiration_month;              //String
    private $expiration_year;               //String
    private $holder_name;                   //String
    private $is_active;                     //Boolean
    private $is_valid;                      //Boolean
    private $is_verified;                   //Boolean
    private $customer;                      //String
    private $fingerprint;                   //String
    private $address;                       //Object
    private $verification_checklist;        //Object
    private $metadata;                      //Object
    private $created_at;                    //String
    private $updated_at;                    //String

    /**
     * Cartao constructor.
     * @param $description
     * @param $card_brand
     * @param $first4_digits
     * @param $expiration_month
     * @param $expiration_year
     * @param $holder_name
     * @param $customer
     * @param $address
     */
    public function __construct($description, $card_brand, $first4_digits, $expiration_month, $expiration_year, $holder_name, $customer, $address)
    {
        $this->description = $description;
        $this->card_brand = $card_brand;
        $this->first4_digits = $first4_digits;
        $this->expiration_month = $expiration_month;
        $this->expiration_year = $expiration_year;
        $this->holder_name = $holder_name;
        $this->customer = $customer;
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCardBrand()
    {
        return $this->card_brand;
    }

    /**
     * @return mixed
     */
    public function getFirst4Digits()
    {
        return $this->first4_digits;
    }

    /**
     * @return mixed
     */
    public function getExpirationMonth()
    {
        return $this->expiration_month;
    }

    /**
     * @return mixed
     */
    public function getExpirationYear()
    {
        return $this->expiration_year;
    }

    /**
     * @return mixed
     */
    public function getHolderName()
    {
        return $this->holder_name;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @return mixed
     */
    public function getIsValid()
    {
        return $this->is_valid;
    }

    /**
     * @return mixed
     */
    public function getIsVerified()
    {
        return $this->is_verified;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return mixed
     */
    public function getFingerprint()
    {
        return $this->fingerprint;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getVerificationChecklist()
    {
        return $this->verification_checklist;
    }

    /**
     * @return mixed
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $card_brand
     */
    public function setCardBrand($card_brand)
    {
        $this->card_brand = $card_brand;
    }

    /**
     * @param mixed $first4_digits
     */
    public function setFirst4Digits($first4_digits)
    {
        $this->first4_digits = $first4_digits;
    }

    /**
     * @param mixed $expiration_month
     */
    public function setExpirationMonth($expiration_month)
    {
        $this->expiration_month = $expiration_month;
    }

    /**
     * @param mixed $expiration_year
     */
    public function setExpirationYear($expiration_year)
    {
        $this->expiration_year = $expiration_year;
    }

    /**
     * @param mixed $holder_name
     */
    public function setHolderName($holder_name)
    {
        $this->holder_name = $holder_name;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }


}