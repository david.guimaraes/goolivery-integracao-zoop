<?php

include_once 'Cartao.php';

class Token
{
    private $id;                            //String
    private $resource;                      //String
    private $used;                          //Boolean
    private $card;                          //Object
    private $created_at;                    //String
    private $updated_at;                    //String

    /**
     * Token constructor.
     * @param $card
     */
    public function __construct($card)
    {
        $this->card = $card;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return mixed
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $card
     */
    public function setCard($card)
    {
        $this->card = $card;
    }

}